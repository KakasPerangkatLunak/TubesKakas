/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kakasperangkatlunak;

/**
 *
 * @author Muhamad Faisal I A
 */
public interface AddAndSubInterface {

    public int add(int n1, int n2);

    public int sub(int n1, int n2);
}
