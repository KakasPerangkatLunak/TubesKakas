/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kakasperangkatlunak;

/**
 *
 * @author TOSHIBA
 */
public interface PowInterface extends MulAndDivInterface {

    public double pow(double n1, double n2);
}
