/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.mycompany.kakasperangkatlunak.B;
import static junit.framework.Assert.*;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ANDARESA
 */
public class TestingKelasB {

    B b = new B();

    @Test
    public void testCase1() {
        double jumlah = b.mul(12, 3);
        assertTrue("Seharusnya hasilnya benar", jumlah == 36);
    }
    @Test
    public void testCase2() {
        double jumlah = b.mul(12, 3);
        assertFalse("Seharusnya hasilnya salah", jumlah == 26);
    }
    @Test
    public void testCase3() {
        double jumlah = b.div(12, 3);
        assertTrue("Seharusnya hasilnya benar", jumlah == 4);
    }
    @Test
    public void testCase4() {
        double jumlah = b.div(12, 3);
        assertFalse("Seharusnya hasilnya salah", jumlah == 5);
    }
    @Test
    public void testCase5() {
        double jumlah = b.pow(12, 3);
        assertTrue("Seharusnya hasilnya benar", jumlah == 4);
    }
    @Test
    public void testCase6() {
        double jumlah = b.pow(12, 3);
        assertFalse("Seharusnya hasilnya salah", jumlah == 5);
    }
}
