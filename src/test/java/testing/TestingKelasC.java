/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.mycompany.kakasperangkatlunak.C;
import static junit.framework.Assert.*;
import org.junit.Test;

/**
 *
 * @author Muhamad Faisal I A
 */
public class TestingKelasC {
    C c = new C();
    
    @Test
    public void testCase1(){
        c.setN1(50);
        double hasil = c.getN1();
        assertNotNull("Seharusnya tidak kosong", c.getN1());
        
    }
    
    @Test
    public void testCase2(){
        c.setN1(10);
        c.setN2(4);
        c.add();
        int hasil = 14;
        assertSame("Seharusnya benar", hasil, c.add());
    }
    
    @Test
    public void testCase3(){
        c.setN1(10);
        c.setN2(4);
        c.add();
        int hasil = 4;
        assertNotSame("Seharusnya benar", hasil, c.add());
    }
}
