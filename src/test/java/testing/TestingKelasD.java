/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;


import com.mycompany.kakasperangkatlunak.D;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ANDARESA
 */
public class TestingKelasD {
    
     D d = new D();

    @Test
    public void testCase1() {
        double jumlah = d.pow(2, 3);
        assertTrue("Seharusnya hasilnya benar", jumlah == 8);
    }
    @Test
    public void testCase2() {
        double jumlah = d.pow(2, 3);
        assertFalse("Seharusnya hasilnya salah", jumlah == 27);
    }
}
