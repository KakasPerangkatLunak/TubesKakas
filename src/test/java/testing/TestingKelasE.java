/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testing;

import com.mycompany.kakasperangkatlunak.E;
import com.mycompany.kakasperangkatlunak.C;
import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.*;
import org.junit.Test;

/**
 *
 * @author ANDARESA
 */
public class TestingKelasE {

    E e = new E();
    List<Object> list = new ArrayList<>();

    @Test
    public void testCase1() {
        list = null;
        e.setDataList(list);
        assertNull("Listnya kosong ", e.getDataList());
    }

    @Test
    public void testCase2() {
        try {
            list = null;
            e.setDataList(list);
            assertTrue("Listnya kosong ", e.getDataList() == null);
        } catch (NullPointerException e) {
            e.getMessage();
        }
    }
}
